var packageName = 'djbuen:vcard-js-sdk';
var where = 'server'; // where to install: 'client' or 'server'. For both, pass nothing.
var version = '1.2.3';
var summary = 'MatchMove Wallet Javascript SDK';
var gitLink = 'git@bitbucket.org:djbuenn/api-vcard-sdk-javascript.git';
var documentationFile = 'readme.md';

Package.describe({
    name: packageName,
    version: version,
    summary: summary,
    git: gitLink,
    documentation: documentationFile
});

Package.onUse(function(api) {
    api.versionsFrom(['METEOR@0.9.0', 'METEOR@1.2.1']); // Meteor versions

    api.addFiles('src/1.3.js','client'); // Files in use
});  
